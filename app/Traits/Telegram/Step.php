<?php


namespace App\Traits\Telegram;


use App\Http\Controllers\Commands\StartCommand;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Step
{
    private function StepHandler()
    {
        $step = $this->user->step;
        if ($step && method_exists(self::class, "step_$step")) {
            $this->admin_permission($step, 'step');
            $this->{"step_$step"}();
            exit();
        }
    }

    private function step_wallet_address()
    {
        if (strlen($this->text) == 34 && str_starts_with($this->text, 'T')) {
            $this->user->wallet_adders = $this->text;
            $this->user->save();
            Telegram::triggerCommand('start', $this->updates, ['new_text' => 'آدرس ولت شما با موفقیت تغییر کرد']);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "آدرس ولت شما باید 34 کارکتر و با T شروع شود",
            ]);
        }
    }

    private function step_withdraw_request()
    {
        if ($this->user->wallet_adders) {
            $amount = floatval($this->text);
            $admin_wallet = get_setting('admin_wallet');
            $private_admin = get_setting('private_admin');
            if ($admin_wallet && $private_admin) {
                $tron = Tron($private_admin);
                if ($tron->fromTron($tron->getBalance($admin_wallet)) >= $amount) {
                    if ($amount > 0 && $this->user->asset >= $amount) {
                        $res = $tron->sendTrx($this->user->wallet_adders, $amount, '', $admin_wallet);
                        if ($res && $res['result'] == true) {
                            $this->user->update([
                                'asset' => $this->user->asset - $amount,
                                'step' => ''
                            ]);
                            Telegram::triggerCommand('start', $this->updates, ['new_text' => 'مبلغ مورد نظر با موفقیت برای شما ارسال گردید']);
                        } else {
                            Telegram::sendMessage([
                                'chat_id' => $this->chat_id,
                                'text' => "در این ساعت واریزی ممکن نمیباشد لطفا چند ساعت دیگه مجددا تلاش کنید",
                            ]);
                        }

                    } else {
                        Telegram::sendMessage([
                            'chat_id' => $this->chat_id,
                            'text' => "مقدار وارد شده از موجودی شما بیشتر میباشد",
                        ]);
                    }
                } else {
                    Telegram::sendMessage([
                        'chat_id' => $this->chat_id,
                        'text' => "در این ساعت واریزی ممکن نمیباشد لطفا چند ساعت دیگه مجددا تلاش کنید",
                    ]);
                    send_message_to_admins("موجودی ولت ادمین کم میباشد لطفا ولت را حداقل $amount شارژ کنید");
                }
            } else {
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => "در این ساعت واریزی ممکن نمیباشد لطفا چند ساعت دیگه مجددا تلاش کنید",
                ]);
                send_message_to_admins('لطفا آدرس ولت و پرایوت ولت را تنظیم کنید.');
            }
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "لطفا قبل از درخواست برداشت آدرس ولت خود را تعیین کنید",
            ]);
        }

    }

    ///////////////////////// ADMIN //////////////////////////////////////
    private function step_change_admin_wallet()
    {
        if (strlen($this->text) == 34 && str_starts_with($this->text, 'T')) {
            set_setting('admin_wallet', $this->text);
            User::update_step();
            $this->inline_admin_wallet(false);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "آدرس ولت شما باید 34 کارکتر و با T شروع شود",
            ]);
        }
    }

    private function step_change_private_admin()
    {
        set_setting('private_admin', trim($this->text));
        User::update_step();
        $this->inline_private_admin(false);
    }

    private function step_change_number_gift()
    {
        if (intval($this->text) > 0) {
            set_setting('number_gift', intval($this->text));
            User::update_step();
            $this->inline_gift(false);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "مقدار وارد شده نامعتبر میباشد",
            ]);
        }
    }

    private function step_change_number_subset()
    {
        if (intval($this->text) > 0) {
            set_setting('number_subset', intval($this->text));
            User::update_step();
            $this->inline_number_subset(false);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "مقدار وارد شده نامعتبر میباشد",
            ]);
        }
    }

    private function step_change_start_text()
    {
        set_setting('start_text', $this->text);
        User::update_step();
        $this->inline_start_text(false);
    }

    private function step_change_referral_text()
    {
        set_setting('referral_text', $this->text);
        User::update_step();
        $this->inline_referral_text(false);
    }

    private function step_change_wallet_text()
    {
        set_setting('wallet_text', $this->text);
        User::update_step();
        $this->inline_wallet_text(false);
    }

    private function step_change_force_join_text()
    {
        set_setting('force_join_text', $this->text);
        User::update_step();
        $this->inline_force_join_text(false);
    }

    private function step_change_channel_join()
    {
        $channels = [];
        foreach (explode("\n", $this->text) as $channel) {
            $channels[] = $channel;
        }
        delete_setting('channel_join');
        insert_setting('channel_join', $channels);
        User::update_step();
        $this->inline_channel_join(false);
    }
}
