<?php


namespace App\Http\Controllers\Commands;

use App\Models\User;
use App\Traits\Telegram\Mr7Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartCommand extends Command
{
    use Mr7Telegram;

    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->set_attributes();
//        Telegram::sendMessage([
//            'chat_id' => 533787134,
//            'text' => 'ssssss'
//        ]);

        $keyboard = [
            [$this->keyboard('wallet_address')],
            [$this->keyboard('withdraw_request')],
            [$this->keyboard('referral_link')],
        ];
        if ($this->user->is_admin) {
            array_unshift($keyboard,[$this->keyboard('admin_panel')]);
        }
        $text = $this->entity['new_text'] ?? get_setting('start_text');

        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => false,
            'one_time_keyboard' => true
        ]);

        $this->replyWithMessage([
            'text' => $text,
            'reply_markup' => $reply_markup
        ]);

        $this->user->update_step();
        exit();
    }
}
