<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//use Laravel\Sanctum\HasApiTokens;
use Telegram\Bot\Laravel\Facades\Telegram;

class User extends Authenticatable
{
    use /*HasApiTokens,*/ HasFactory, Notifiable;
    protected $guarded= [];

    public function getReferralLinkAttribute()
    {
        $bot= Telegram::setAsyncRequest(false)->getMe();
        $bot_username= $bot['username'];
        return "https://t.me/$bot_username?start=referral=$this->referral";
    }

    public function parent()
    {
        return $this->belongsTo(User::class,'parent_id','id');
    }

    public function subset()
    {
        return $this->hasMany(User::class,'parent_id','id');
    }

    public static function update_step($step='') {
        $user_id = request()->has('callback_query') ? request()->callback_query['from']['id'] : request()->message['from']['id'];
        return self::query()->where('bot_id',$user_id)
            ->update([
                'step' => $step
            ]);
    }

    public function scopeAdmin($q)
    {
        return $q->where('is_admin',1);
    }
}
