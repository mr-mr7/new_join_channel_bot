<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bot_id');
            $table->string('en_id')->nullable();
            $table->string('name')->nullable();
            $table->unsignedFloat('asset')->default(0);
            $table->string('wallet_adders')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('referral');
            $table->integer('new_subset')->default(0);
            $table->tinyInteger('is_admin')->default(0);
            $table->tinyInteger('status')->default(0)->comment('-1=>block 0=>new 1=>joined');
            $table->string('step')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')
                ->on('users')
                ->references('id')
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
