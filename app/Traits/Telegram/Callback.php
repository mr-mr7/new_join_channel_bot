<?php


namespace App\Traits\Telegram;


use App\Http\Controllers\Commands\StartCommand;
use App\Models\User;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Callback
{
    private function CallbackHandler()
    {
        $method = $this->key_keyboard($this->text);
        if ($method && method_exists(self::class, $method)) {
            $this->admin_permission($method, 'keyboard');
            User::update_step('');
            $this->{$method}();
            exit();
        }
    }

    private function wallet_address()
    {
        $wallet_adders = $this->user->wallet_adders;
        if ($wallet_adders) {
            $text = get_setting('wallet_text')."\n";
            $text .= "<code>$wallet_adders</code>";
            $inline_text= 'ویرایش آدرس ولت';
        } else {
            $text = "شما هیچ آدرس ولت تعریف نکرده اید";
            $inline_text= 'تعیین آدرس ولت';
        }
        $keyboard = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => "$inline_text", 'callback_data' => 'wallet_address']),
            );
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => "$text",
            'reply_markup' => $keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

    private function referral_link()
    {
        $inline_text = $text = "\n ➖➖➖➖➖➖➖➖➖ \n" . get_setting('referral_text') . "\n";
        $text .= "`".$this->user->referral_link . "`";
        $inline_text .= $this->user->referral_link;

        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "ارسال به دوستان", "switch_inline_query" => "$inline_text"]));

        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
            'parse_mode' => 'markDown'
        ]);
    }

    private function withdraw_request()
    {
        if ($this->user->wallet_adders) {
            $text = "موجودی شما: " . $this->user->asset . "\n";
            $text .= "جهت ثبت درخواست لطفا مقدار ترون برای برداشت را مشخص کنید" . "\n";
            $text .= "مثال: 5";
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "$text",
                'parse_mode' => 'HTML'
            ]);
            User::update_step('withdraw_request');
        }else {
            Telegram::triggerCommand('start', $this->updates,['new_text'=>'لطفا ابتدا آدرس ولت خود را تعیین کنید سپس اقدام به درخواست برداشت کنید']);
        }
    }

    public function start()
    {
        Telegram::triggerCommand('start', $this->updates);
    }

    ///////////////////////// ADMIN //////////////////////////////////////
    private function admin_panel()
    {
        $keyboard = [
            [$this->keyboard('start')],
            [$this->keyboard('bot_settings')],
        ];

        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => false,
            'one_time_keyboard' => true,
        ]);
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'به پنل مدیریت خوش آمدید',
            'reply_markup' => $reply_markup
        ]);

    }

    private function bot_settings($edit = false)
    {
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "وضعیت ربات", 'callback_data' => "bot_status"]))
            ->row(Keyboard::inlineButton(['text' => "آدرس ولت ادمین", 'callback_data' => "admin_wallet"]), Keyboard::inlineButton(['text' => "پرایوت ولت ادمین", 'callback_data' => "private_admin"]))
            ->row(Keyboard::inlineButton(['text' => "تعداد زیرمجموعه", 'callback_data' => "number_subset"]), Keyboard::inlineButton(['text' => "تعداد جایزه", 'callback_data' => "number_gift"]))
            ->row(Keyboard::inlineButton(['text' => "کانال جوین اجباری", 'callback_data' => "channel_join"]))
            ->row(Keyboard::inlineButton(['text' => "متن استارت", 'callback_data' => "start_text"]), Keyboard::inlineButton(['text' => "متن رفرال ", 'callback_data' => "referral_text"]))
            ->row(Keyboard::inlineButton(['text' => "متن کیف پول ", 'callback_data' => "wallet_text"]), Keyboard::inlineButton(['text' => "متن جوین اجباری", 'callback_data' => "force_join_text"]));
        if ($edit == false) {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "تنظیمات ربات",
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => "تنظیمات ربات",
                'reply_markup' => $keyboard,
            ]);
        }
    }

    /*
        private function login()
        {
            if ($this->is_login) {
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => 'شما قبلا وارد شده اید'
                ]);
                Telegram::triggerCommand('start', $this->updates);
                exit();
            }
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'جهت ورود شماره خود را وارد کنید'
            ]);
            User::update_step('send_verify_sms');
        }

        private function register()
        {
            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'ثبت نام در سایت', 'url' => 'https://hani.gold/user/register']),
                );
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'لطفا برای ثبت نام روی باتن زیر کلیک کنید',
                'reply_markup' => $keyboard
            ]);
        }

        private function orders_list()
        {
            $user_id = $this->user_id;
            $orders = Order::query()
                ->whereHas('user.bot_users', function ($q) use ($user_id) {
                    $q->where('userbot_id', $user_id);
                })
                ->with('product')
                ->take(10)->get();
            if ($orders->count() > 0) {
                $text = "<b>لیست سفارشات اخیر شما: </b> \n";
                foreach ($orders as $key => $order) {
                    $price = number_format($order->price);
                    $text .= ($key + 1) . ") <b>{$order->status_text}:</b> {$order->type} {$order->amount} {$order->product->unit} {$order->product->name} با قیمت {$price} ریال ";
                    $text .= "\n";
                }
                $text .= ' .';
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => $text,
                    'parse_mode' => "HTML"
                ]);

                $keyboard = Keyboard::make()
                    ->inline()
                    ->row(
                        Keyboard::inlineButton(['text' => 'مشاهده لیست کامل سفارشات', 'url' => 'https://hani.gold/api/v1/user/orders']),
                    );
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => 'برای مشاهده لیست کامل سفارشات روی باتن زیر کلیک کنید',
                    'reply_markup' => $keyboard
                ]);


            } else {
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => 'در حال حاضر سفارشی برای شما ثبت نشده است'
                ]);
            }
        }

        private function track_order()
        {

        }

        private function contact_us()
        {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'در اینجا متن و اطلاعات تماس با ما نوشته میشود'
            ]);
        }

        private function about_us()
        {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'در اینجا متن درباره ما نوشته میشود'
            ]);
        }

        private function logout()
        {
            BotUser::query()->where('userbot_id', $this->user_id)->update([
                'step' => '',
                'user_id' => NULL
            ]);
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'شما با موفقیت خارج شدید'
            ]);
            Telegram::triggerCommand('start', $this->updates);
        }
    */
}
