<?php

namespace App\Traits\Telegram;

use App\Models\User;
use Illuminate\Support\Str;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Mr7Telegram
{
    protected $user_id, $chat_id, $message_id, $text, $user, $callbak_data, $callbak_id, $en_id, $full_name;

    // ست کردن اتریبیوت های کاربردی در ربات
    private function set_attributes()
    {
        if (request()->has('callback_query')) { // inline button
            $this->user_id = request()->callback_query['from']['id'];
            $this->en_id = request()->callback_query['from']['username'];
            $this->full_name = request()->callback_query['from']['first_name'] ?? '' . request()->callback_query['from']['last_name'] ?? '';
            $this->chat_id = request()->callback_query['message']['chat']['id'];
            $this->message_id = request()->callback_query['message']['message_id'];
            $this->text = request()->callback_query['message']['text'];

            $this->callbak_data = request()->callback_query['data'];
            $this->callbak_id = request()->callback_query['id'];
        } else {
            $this->user_id = request()->message['from']['id'];
            $this->en_id = request()->message['from']['username'];
            $this->full_name = request()->message['from']['first_name'] ?? '' . request()->message['from']['last_name'] ?? '';
            $this->chat_id = request()->message['chat']['id'];
            $this->message_id = request()->message['message_id'];
            $this->text = request()->message['text'];
        }

        $this->set_user();
    }

    private function keyboards_array()
    {
        return [
            'wallet_address' => 'آدرس ولت شما',
            'referral_link' => 'لینک دعوت دوستان',
            'withdraw_request' => 'درخواست برداشت',

            //admin
            'start' => 'منوی اصلی',
            'admin_panel' => 'پنل مدیریت',
            'bot_settings' => 'تنظیمات سامانه'
        ];
    }

    /*
     * $type => اینکه اینلاین کیبورد هست یا کیبورد عادی یا مرحله
     * $val => اینکه چه اینلاینی یا یه کیبوردی یا چه مرحله ای هست
     */
    private function admin_permission($val, $type)
    {
        $permission = [
            'keyboard' => [
                'admin_panel', 'bot_settings'
            ],
            'inline' => [
                'bot_status', 'change_bot_status', 'admin_wallet', 'change_admin_wallet', 'number_subset', 'change_number_subset', 'number_gift', 'change_number_gift',
                'start_text', 'change_start_text', 'referral_text', 'change_referral_text', 'wallet_text', 'change_wallet_text', 'channel_join', 'change_channel_join'
            ],
            'step' => [
                'change_admin_wallet', 'change_number_subset', 'change_number_gift', 'change_start_text', 'change_referral_text', 'change_wallet_text', 'change_channel_join'
            ],
        ];

        if (in_array($val, $permission["$type"])) {
            if ($this->user->is_admin != 1) {
                Telegram::triggerCommand('start', $this->updates, ['new_text' => 'شما به این بخش دسترسی ندارید']);
                exit("");
            }
        }


    }

    // این تابع برای گرفتن نام فارسی دکمه های ربات میباشد
    public function keyboard($key)
    {
        $array = $this->keyboards_array();
        return $array[$key] ?? '';
    }

    // این تابع ما مقدار فارسی بهش میدیم و اون key اون رو بهمون میده
    public function key_keyboard($val)
    {
        $array = array_flip($this->keyboards_array());
        return $array[$val] ?? '';
    }

    // ثبت ربات اگه تازه اومده
    private function set_user()
    {
        $user = User::query()
            ->where('bot_id', $this->user_id)
            ->first();
        if ($user) {
            $this->user = $user;
        } else {
            $parent_id = $this->check_referral();
            $this->user = User::query()->create([
                'bot_id' => $this->user_id,
                'en_id' => $this->en_id,
                'name' => $this->full_name,
                'referral' => strtoupper(Str::random('8')),
                'parent_id' => $parent_id,
            ]);
        }
    }

    private function check_referral()
    {
        $referral = $this->get_query('referral');
        if ($referral) {
            return User::query()->where('referral', $referral)->value('id');
        }
        return null;
    }

    public function get_query($key)
    {
        $queries = $this->start_query();
        return $queries["$key"] ?? null;
    }

    private function start_query(): array
    {
        $array = array();
        if (str_starts_with($this->text, '/start')) {
            $query = explode(' ', $this->text);
            if (isset($query[1])) {
                $pairs = explode('&', $query[1]);
                foreach ($pairs as $pair) {
                    $temp = explode("=", $pair);
                    $array[$temp[0]] = $temp[1] ?? null;
                }
            }
        }
        return $array;
    }

    // بررسی روشن یا خاموش بودن ربات
    private function check_status()
    {
        if ($this->user->is_admin != '1') {
            if (get_setting('bot_status') == 'خاموش') {
                Telegram::sendMessage([
                    'chat_id' => $this->chat_id,
                    'text' => "در حال حاضر ربات خاموش میباشد",
                    'parse_mode' => 'HTML'
                ]);
                exit('');
            }
        }
    }

}
