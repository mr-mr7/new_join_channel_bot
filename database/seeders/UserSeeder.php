<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create([
            'bot_id' => 533787134,
            'en_id' => 'mr_mr7',
            'name' => 'محمدرضا نادری',
            'is_admin' => 1,
            'referral' => strtoupper(Str::random('8')),
        ]);
    }
}
