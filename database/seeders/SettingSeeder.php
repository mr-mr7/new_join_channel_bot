<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::query()->insert([
            ['name' => 'bot_status', 'value' => 'روشن'],
            ['name' => 'admin_wallet', 'value' => ''],
            ['name' => 'private_admin', 'value' => ''],
            ['name' => 'number_gift', 'value' => '10'],
            ['name' => 'number_subset', 'value' => '10'],
            ['name' => 'start_text', 'value' => 'متن استارت'],
            ['name' => 'referral_text', 'value' => 'لینک دعوت شما'],
            ['name' => 'wallet_text', 'value' => 'متن کیف پول'],
            ['name' => 'force_join_text', 'value' => 'متن جوین اجباری'],
            ['name' => 'channel_join', 'value' => ''],
        ]);
    }
}
