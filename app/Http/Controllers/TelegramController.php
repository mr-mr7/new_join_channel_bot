<?php

namespace App\Http\Controllers;

use App\Traits\Telegram\Callback;
use App\Traits\Telegram\Inline;
use App\Traits\Telegram\Mr7Telegram;
use App\Traits\Telegram\Step;
use Illuminate\Http\Request;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Methods\EditMessage;

//https://api.telegram.org/bot123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11/getMe
//https://75a5-194-50-233-242.eu.ngrok.io


class TelegramController extends Controller
{
    use Mr7Telegram, Callback, Inline, Step;

    private $updates;

    public function webhook_updates()
    {
        $this->updates = Telegram::getWebhookUpdates();
        $this->set_attributes();
        $this->check_join();
        $this->check_status();
        Telegram::commandsHandler(true);
        $this->CallbackHandler();
        $this->InlineHandler();
        $this->StepHandler();
    }

    public function urls()
    {
        $token = env('TELEGRAM_BOT_TOKEN');
        if (\request()->has('token') && \request()->token ==$token) {
            $site_url = env('SITE_URL')."/$token/webhook";
            echo "<a target='_blank' href='https://api.telegram.org/bot{$token}/getMe'>getMe</a><br>";
            echo "<a target='_blank' href='https://api.telegram.org/bot{$token}/setWebhook?url=$site_url'>setWebhook</a><br>";
            echo "<a target='_blank' href='https://api.telegram.org/bot{$token}/deleteWebhook'>deleteWebhook</a><br>";
            echo "<a target='_blank' href='https://api.telegram.org/bot{$token}/getWebhookInfo'>getWebhookInfo</a><br>";
        }else {
            abort("404");
        }
    }

    private function check_join()
    {
        $channels = get_setting('channel_join', false);
        $keyboard = Keyboard::make()->inline();
        $join = true;
        foreach ($channels as $item) {
            try {
                $check_join = \Telegram\Bot\Laravel\Facades\Telegram::getChatMember(['chat_id' => "@{$item->value}", 'user_id' => $this->user_id]);
                if (!$check_join || $check_join['status'] == "left" || $check_join['status'] == "kicked") {
                    $join = false;
                    $keyboard->row(Keyboard::inlineButton(['text' => "$item->value", 'url' => "https://t.me/$item->value"]));
                }
            } catch (\Exception $e) {

            }
        }
        if (request()->has('callback_query') && request()->callback_query['data'] == 'check_join') {
            Telegram::deleteMessage(['chat_id' => request()->callback_query['message']['chat']['id'], 'message_id' => request()->callback_query['message']['message_id']]);
        }

        if ($join == false) {
            $keyboard->row(Keyboard::inlineButton(['text' => "عضو شدم", 'callback_data' => "check_join"]));
            $text = get_setting('force_join_text');
            Telegram::sendMessage([
                'chat_id' => $this->user_id,
                'text' => "$text",
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
            exit("");
        } else {
            if ($this->user->status == '0') {
                $number_join = get_setting('number_subset');
                $gift = get_setting('number_gift');
                $this->user->update(['status' => 1]);
                $parent = $this->user->parent;
                if ($parent) {
                    if ($number_join <= ($parent->new_subset + 1)) {
                        $new_asset = $parent->asset + $gift;
                        $parent->update([
                            'new_subset' => ($parent->new_subset + 1) - $number_join,
                            'asset' => $new_asset
                        ]);
                        Telegram::sendMessage([
                            'chat_id' => $parent->bot_id,
                            'text' => "یک نفر به تعداد زیر مجموعه های شما اضافه شد و موجودی شما {$gift} ترون افزایش یافت. موجودی شما : {$new_asset}",
                        ]);
                    } else {
                        $new_subset = $parent->new_subset + 1;
                        $parent->update(['new_subset' => $new_subset]);
                        Telegram::sendMessage([
                            'chat_id' => $parent->bot_id,
                            'text' => "یک نفر به تعداد زیر مجموعه های شما اضافه شد. تعداد زیرمجموعه های شما: {$new_subset}",
                        ]);
                    }
                }
            }
        }
    }
}
