<?php

use Telegram\Bot\Laravel\Facades\Telegram;

function delete_setting($name)
{
    return \App\Models\Setting::query()->where('name', "$name")->delete();
}

function get_setting($name, $first = true)
{
    if ($first == true) {
        return \App\Models\Setting::query()->where('name', "$name")->value('value');
    } else {
        return \App\Models\Setting::query()->where('name', "$name")->get();
    }
}

function set_setting($name, $value)
{
    return \App\Models\Setting::query()->where('name', "$name")->update(['value' => "$value"]);
}

function insert_setting($name, $value)
{
    if (is_array($value)) {
        $insert = [];
        foreach ($value as $item) {
            $insert[] = ['name' => "$name", 'value' => "$item", 'created_at' => now(), 'updated_at' => now()];
        }
        return \App\Models\Setting::query()->insert($insert);
    } else {
        return \App\Models\Setting::query()->create(['name' => "$name", 'value' => "$value"]);
    }
}

function send_message_to_admins($message)
{
    $admins = \App\Models\User::query()->admin()->get(['id', 'bot_id']);
    foreach ($admins as $admin) {
        Telegram::sendMessage([
            'chat_id' => $admin->bot_id,
            'text' => "$message",
        ]);
    }
}
