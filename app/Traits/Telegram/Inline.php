<?php


namespace App\Traits\Telegram;


use App\Models\User;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Inline
{
    private function InlineHandler()
    {
        if ($this->callbak_data) {
            $params = explode('#', $this->callbak_data);
            $method = $params[0];
            if ($method && method_exists(self::class, "inline_$method")) {
                $this->admin_permission($method, 'inline');
                User::update_step();
                unset($params[0]);
                $this->{"inline_$method"}();
                exit();
            }
        }
    }

    private function inline_wallet_address()
    {
        $text = "آدرس ولت ترون خود را وارد کنید  \n";
        Telegram::editMessageText([
            'chat_id' => $this->chat_id,
            'message_id' => $this->message_id,
            'text' => $text
        ]);
        User::update_step('wallet_address');
    }

    private function inline_check_join()
    {
        Telegram::triggerCommand('start', $this->updates);
    }

    ///////////////////////// ADMIN //////////////////////////////////////
    private function inline_back_bot_setting()
    {
        User::update_step();
        $this->bot_settings(true);
    }

    private function inline_bot_status()
    {
        $bot_status = get_setting('bot_status');
        $text = "وضعیت ربات : ";
        $text .= $bot_status == 'روشن' ? '⚪️ روشن' : '🔴 خاموش';
        $inline_text = $bot_status == 'روشن' ? '🔴 خاموش کردن' : '⚪️ روشن کردن';
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "$inline_text", 'callback_data' => "change_bot_status"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'chat_id' => $this->chat_id,
            'message_id' => $this->message_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
    }

    private function inline_change_bot_status()
    {
        $bot_status = get_setting('bot_status');
        $new_status = $bot_status == 'روشن' ? 'خاموش' : 'روشن';
        set_setting('bot_status', "$new_status");
        $this->inline_bot_status();
    }

    private function inline_admin_wallet($edit = true)
    {
        $admin_wallet = get_setting('admin_wallet');
        $text = "آدرس ولت ادمین : " . "\n";
        $text .= "<code>$admin_wallet</code>";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر ادرس ولت", 'callback_data' => "change_admin_wallet"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_admin_wallet()
    {
        $text = "لطفا آدرس ولت جدید را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_admin_wallet');
    }

    private function inline_private_admin($edit = true)
    {
        $private_admin = get_setting('private_admin');
        $text = " پرایوت ولت ادمین : " . "\n";
        $text .= "<code>$private_admin</code>";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر پرایوت ولت", 'callback_data' => "change_private_admin"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_private_admin()
    {
        $text = "لطفا پرایوت جدید ولت را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_private_admin');
    }

    private function inline_number_gift($edit = true)
    {
        $gift = get_setting('number_gift');
        $text = "تعداد جایز : " . "\n";
        $text .= "<code>$gift</code>";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر تعداد جایزه", 'callback_data' => "change_number_gift"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_number_gift()
    {
        $text = "لطفا تعداد جایزه را وارد کنید. مثال: 5";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_number_gift');
    }

    private function inline_number_subset($edit = true)
    {
        $number_subset = get_setting('number_subset');
        $text = "تعداد زیرمجموعه : " . "\n";
        $text .= "<b>$number_subset</b>";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر تعداد زیرمجموعه", 'callback_data' => "change_number_subset"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_number_subset()
    {
        $text = "لطفا تعداد زیر مجموعه را وارد کنید. مثال: 5";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_number_subset');
    }

    private function inline_start_text($edit = true)
    {
        $start_text = get_setting('start_text');
        $text = "متن استارت ربات : " . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖" . "\n";
        $text .= "$start_text";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر متن استارت", 'callback_data' => "change_start_text"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_start_text()
    {
        $text = "متن جدید برای استارت ربات را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_start_text');
    }

    private function inline_referral_text($edit = true)
    {
        $referral_text = get_setting('referral_text');
        $text = "متن نمایش لینک رفرال : " . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖" . "\n";
        $text .= "$referral_text";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر متن رفرال", 'callback_data' => "change_referral_text"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_referral_text()
    {
        $text = "متن جدید نمایش لینک رفرال را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_referral_text');
    }

    private function inline_wallet_text($edit = true)
    {
        $wallet_text = get_setting('wallet_text');
        $text = "متن کیف پول کاربر : " . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖" . "\n";
        $text .= "$wallet_text";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر متن کیف پول", 'callback_data' => "change_wallet_text"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_wallet_text()
    {
        $text = "متن جدید کیف پول کاربر را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_wallet_text');
    }

    private function inline_force_join_text($edit = true)
    {
        $force_join_text = get_setting('force_join_text');
        $text = "متن جوین اجباری : " . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖" . "\n";
        $text .= "$force_join_text";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر متن جوین اجباری", 'callback_data' => "change_force_join_text"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_force_join_text()
    {
        $text = "متن جدید جوین اجباری را وارد کنید";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_force_join_text');
    }

    private function inline_channel_join($edit = true)
    {
        $channel_join = get_setting('channel_join', false);
        $text = "کانال های جوین اجباری : " . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖" . "\n";
        foreach ($channel_join as $item)
            $text .= "@{$item->value}" . "\n";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "تغییر کانال جوین اجباری", 'callback_data' => "change_channel_join"]))
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        if ($edit == true) {
            Telegram::editMessageText([
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
        }
    }

    private function inline_change_channel_join()
    {
        $text = " کانال های جوین اجباری را وارد کنید."."\n";
        $text .= "1- ایدی کانال حتما بدون @ نوشته شود." . "\n";
        $text .= "2- در هر خط ایدی یک کانال را وارد کنید." . "\n";
        $text .= "3- ربات باید در کانال مورد نظر نقش مدیریت داشته باشه." . "\n";
        $keyboard = Keyboard::make()->inline()
            ->row(Keyboard::inlineButton(['text' => "بازگشت", 'callback_data' => "back_bot_setting"]));
        Telegram::editMessageText([
            'message_id' => $this->message_id,
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        User::update_step('change_channel_join');
    }
}
